# SDF models and world of RoboBoat for Gazebo

We want to define each task independently, so we can test ASV with given task. Later we want to combine them into a single world.

## Getting started

Dependencies:
* Gazebo 9
* Python

For interacting with auto generation scripts, refer to [instructions in scripts folder](./scripts/README.md)

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Contributing

We are using SDF 1.7

### Adding models
Each model should have name as defined in https://outline.simle.pl/doc/mapping-simulation-xwtMWkXNhU
We want to create models for each object. So we can reuse them, or link them in world files. If we change anything within a model, we want it reflected in the world without changing much.

### Adding worlds
You can create them using Gazebo editor. But if it contains a lot of objects, generating them would be preffered, especially for task 2.

## Authors and acknowledgment
* Norbert Szulc

## License
TBD

## Project status
Development will stop after RoboBoat 2023, unless we will attempt next year.
